# 零股倉 委託管理 不分頁，一次顯示全部委託
>委託管理頁面載入時，自動切換分頁筆數為 10000 筆，也就是永遠只會有一頁，所有資料都會一次呈現。

* 此為 Tampermonkey 腳本，Tampermonkey 安裝及腳本新增方式請參考 [Tampermonkey 安裝及腳本新增方式](https://gitlab.com/swtsaicain/stockhouse)。
* 腳本檔案：[零股倉 委託管理 不分頁，一次顯示全部委託.txt](零股倉 委託管理 不分頁，一次顯示全部委託.txt)
* 使用圖  
  ![image.png](./image.png)

  ![image-1.png](./image-1.png)
* 此腳本適用零股倉所有分頁顯示頁面，只要將 @match 後之 url 做對應調整即可。
* 如果想要全站適用，直接把 match 改成下面這行。
  ```
  // @match        https://*.stockhouse.com.tw/*
  ```
  
