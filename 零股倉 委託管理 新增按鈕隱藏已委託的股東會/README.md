# 零股倉 委託管理 新增按鈕隱藏已委託的股東會
>委託管理頁面載入時，新增"隱藏已委託的股東會"按鈕，點擊可將所有已委託數大於 0 的股東會隱藏。

* 此為 Tampermonkey 腳本，Tampermonkey 安裝及腳本新增方式請參考 [Tampermonkey 安裝及腳本新增方式](https://gitlab.com/swtsaicain/stockhouse)。
* 腳本檔案：[零股倉 委託管理 新增按鈕隱藏已委託的股東會.txt](零股倉 委託管理 新增按鈕隱藏已委託的股東會.txt)
* 使用方式
  * 新增按鈕  
    ![image-1.png](./image-1.png)
  * 點擊按鈕後，所有已委託數大於 0 的股東會隱藏。
    ![image-2.png](./image-2.png)

