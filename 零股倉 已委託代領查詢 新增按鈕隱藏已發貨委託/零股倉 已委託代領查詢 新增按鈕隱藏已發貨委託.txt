// ==UserScript==
// @name         零股倉 已委託代領查詢 新增按鈕隱藏已發貨委託
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  零股倉 已委託代領查詢 新增按鈕隱藏已發貨委託
// @author       Cain
// @match        https://*.stockhouse.com.tw/viewlog.php?z=1
// @icon         https://www.google.com/s2/favicons?sz=64&domain=stockhouse.com.tw
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    $('body').append('<button id="hide1" style="z-index: 9999; position: fixed ! important; right: 0px; top: 0px;">隱藏已發貨委託</button>');
    $('#hide1').click(function() {
        var $rows = $('tbody tr').filter(function() {
            var amount = $(this).children('td:nth-child(5)').text();
            return ( amount != "0" );
        });
        $rows.hide();
    });

    var myInterval = setInterval(function() {
        var $pageCount = $('.dataTables_length select');
        if ($pageCount != undefined) {
            $pageCount.append('<option value="10000">10000</option>');
            $pageCount.val("10000").trigger('change');
            clearInterval(myInterval);
        }
    }, 1000)
})();