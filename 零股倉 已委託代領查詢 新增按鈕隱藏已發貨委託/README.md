# 零股倉 已委託代領查詢 新增按鈕隱藏已發貨委託
>委託管理頁面載入時，自動切換分頁筆數為 10000 筆，也就是永遠只會有一頁，所有資料都會一次呈現。

* 此為 Tampermonkey 腳本，Tampermonkey 安裝及腳本新增方式請參考 [Tampermonkey 安裝及腳本新增方式](https://gitlab.com/swtsaicain/stockhouse)。
* 腳本檔案：[零股倉 已委託代領查詢 新增按鈕隱藏已發貨委託.txt](零股倉 已委託代領查詢 新增按鈕隱藏已發貨委託.txt)
* 使用圖  
  ![image.png](./image.png)
* 此腳本包含了一頁顯示全部委託功能。