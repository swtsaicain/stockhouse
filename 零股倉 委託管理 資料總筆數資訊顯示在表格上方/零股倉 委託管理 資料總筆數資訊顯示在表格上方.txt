// ==UserScript==
// @name         零股倉 委託管理 資料總筆數資訊顯示在表格上方
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  零股倉 委託管理 資料總筆數資訊顯示在表格上方
// @author       Cain
// @match        https://*.stockhouse.com.tw/viewlog.php?z=5
// @icon         https://www.google.com/s2/favicons?sz=64&domain=stockhouse.com.tw
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    var myInterval2  = setInterval(function() {
        var $tableInfo = $("#meeting-table_info");
        var $meetingTable = $("#meeting-table");
        if ($tableInfo != undefined && $meetingTable != undefined) {
            var tableInfo = $tableInfo.text()
            console.log(tableInfo);
            console.log($tableInfo.outerHTML);
            $meetingTable.before($tableInfo[0].outerHTML);
            clearInterval(myInterval2);
        }
    }, 1000)
})();
