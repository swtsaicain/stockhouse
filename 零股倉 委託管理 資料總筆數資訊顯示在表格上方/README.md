# 零股倉 委託管理 資料總筆數資訊顯示在表格上方
>委託管理頁面載入時，資料總筆數顯示在表格上方，可以直接看到可委託公司總數是否有異動。

* 此為 Tampermonkey 腳本，Tampermonkey 安裝及腳本新增方式請參考 [Tampermonkey 安裝及腳本新增方式](https://gitlab.com/swtsaicain/stockhouse)。
* 腳本檔案：[零股倉 委託管理 資料總筆數資訊顯示在表格上方](零股倉 委託管理 資料總筆數資訊顯示在表格上方.txt)
* 使用圖  
  ![image.png](./image.png)
* 此腳本適用零股倉所有分頁顯示頁面，只要將 @match 後之 url 做對應調整即可。